package com.zuitt.WDC043_S5_A1;

public class Main {

    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        Contact contact2 = new Contact();

        contact1.setName("John Doe");
        contact1.setNumbers("+639123456789");
        contact1.setNumbers("+639987654321");
        contact1.setAddresses("Makati City");
        contact1.setAddresses("Quezon City");

        contact2.setName("Jane Doe");
        contact2.setNumbers("+639102938475");
        contact2.setNumbers("+639564738292");
        contact2.setAddresses("Marikina City");
        contact2.setAddresses("Mandaluyong City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.getContacts();

    }
}
