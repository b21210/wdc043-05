package com.zuitt.WDC043_S5_A1;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    public Contact(){}

    public Contact(String name, String number, String address){
        this.name = name;
        this.numbers.add(number);
        this.addresses.add(address);
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<String> getNumbers(){
        return this.numbers;
    }

    public ArrayList<String> getAddresses(){
        return this.addresses;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setNumbers(String number){
        this.numbers.add(number);
    }

    public void setAddresses(String address) {
        this.addresses.add(address);
    }

    public void details(){
        System.out.println(this.name);
        System.out.println("---------------------");
        System.out.println(this.name + " has the following registered numbers:");
        for(String num: this.numbers){
            System.out.println(num);
        }
        System.out.println("--------------------------------------");
        System.out.println(this.name + " has the following registered addresses");
        for(int i = 0; i < this.addresses.size(); i++){
            if(i == 0){
                System.out.println("my home in " + this.addresses.get(i));
            }
            else if(i == 1){
                System.out.println("my office in " + this.addresses.get(i));
            }
            else{
                System.out.println("my other addresses:");
                System.out.println(this.addresses.get(i));
            }
        }
        System.out.println("======================================");
    }

}
