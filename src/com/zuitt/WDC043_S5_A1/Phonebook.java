package com.zuitt.WDC043_S5_A1;

import java.util.ArrayList;

public class Phonebook{

    ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){}

    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }


    public void getContacts(){
        if(contacts.size() != 0){
//            return contacts.details();
            for(Contact cont: contacts){
                cont.details();
            }
        }
        else {
            System.out.println("Empty");
        }
    }

    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }

}
